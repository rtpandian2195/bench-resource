const jwt = require('jsonwebtoken')

require('dotenv').config()



const GenerateToken = (data) => {
    const token = jwt.sign(data, process.env.JWT_TOKEN, {expiresIn : "1h"})

    return token;
}

const GenerateRefreshToken = (data) => {
    const token = jwt.sign(data, process.env.JWT_REFRESH_TOKEN, {expiresIn : "1d"})

    return token;
}

const ExtractToken = (data) => {
    const result = jwt.verify(data, process.env.JWT_REFRESH_TOKEN)

    console.log(result);

    return result;

   
}



module.exports = {
    GenerateToken,
    GenerateRefreshToken,
    ExtractToken
}