const express  =require('express')
const router = express.Router()

const {upload} = require('../helper/filehelper')

const {createCategory,getCategory,getDetailCategory,UpdateCategory,DeleteCategory, getAllCategory} = require('../controller/category')

const {createSubCategory,getSubCategory,getDetailSubCategory,UpdateSubCategory,DeleteSubCategory} = require('../controller/sub_category')
const {createPost,Userpost } = require('../controller/post')

const {bestData, topbest} = require('../controller/best')

const {login,RefreshToken,UserLogout, UserDetail} = require('../controller/user')

const {Postdata,getData,getDataall, UpdateData, DeleteData, Topdata, TagData, getDate} = require('../controller/auto')

const {auth} = require('../middleware/authorization')

const Validation = require('../middleware/validation/Validation')


//category
router.post('/category',Validation.categoryValidation, createCategory)
router.get('/category', getCategory)
router.get('/category/:id', getDetailCategory)
router.patch('/category/:id', UpdateCategory)
router.delete('/category/:id', DeleteCategory)

router.get('/categoryAll', getAllCategory)

//sub-category
router.post('/subcategory',Validation.subCategoryValidation, createSubCategory)
router.get('/subcategory', getSubCategory)
router.get('/subcategory/:id', getDetailSubCategory)
router.patch('/subcategory/:id', UpdateSubCategory)
router.delete('/subcategory/:id', DeleteSubCategory)



//login
router.post('/login',Validation.loginValidation, login)
router.get('/refreshToken', RefreshToken)
router.get('/user', auth, UserDetail)
router.get('/logout', auth, UserLogout)

//post
router.post('/post',upload.single('image'), createPost)
router.get('/post',auth,Userpost)


//post-data
router.post('/data', Postdata)
router.get('/getData', getData )
router.get('/data', getDataall)
router.patch('/data/:id', UpdateData )
router.delete('/data/:id', DeleteData)
router.get('/Top', Topdata)
router.get('/Tag-filter', TagData)
router.get('/tag-date', getDate)

//Best
router.post('/best', topbest)
router.get('/best', bestData)








module.exports = router
