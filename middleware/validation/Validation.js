const Validator = require('validatorjs')
const category = require('../../models/category')
const sub_category = require('../../models/sub_category')
//const db = require('../models')


const loginValidation = async(req,res,next) => {
    try {
        const {email, password} = req.body

        const data = {
            email,
            password
        }

        const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]/;

        // Tighten password policy
        Validator.register('strict', value => passwordRegex.test(value),
            'password must contain at least one uppercase letter, one lowercase letter and one number');


        const rules = {
            "email" : "required|email",
            "password" : "required|string|min:6|strict"
        }

        const validate = new Validator(data,rules)
        if(validate.fails()) {
            return  res.status(400).json({
                message : validate.errors
            })
        }
        next()
    } catch (error) {
        res.status(400).send(error.message)
    }
}

const categoryValidation = async(req,res,next) => {
    try {
        const {name} = req.body

        const data = {name}

        console.log(data);

        const rules = {
            "name" : "required|string|max:50"
        }

        const validate = new Validator(data,rules)
        if(validate.fails()) {
            return  res.status(400).json({
                message : validate.errors
            })
        }
        next()
    } catch (error) {
        res.status(400).send(error.message)
    }
}

const subCategoryValidation = async(req,res,next) => {
    try {
        const {name, category_id} = req.body

        const data = {
            name,
            category_id
        }

        

        const rules = {
            "name" : "required|string|max:50",
            "category_id" : "required|integer"
        }
        
        const validate = new Validator(data,rules)

        if(validate.fails()) {
            return res.status(400).json({
                message : validate.errors
            })
        }

        const Id = await category.findOne({
            where : {
                id : category_id
            }
        })

        if(!Id) {
            const errorData = {
                errors : {
                    categoryId : [
                        "categoryId Not found"
                    ]
                }
            }
            return res.status(400).json({
                message : errorData
            })
        }
        next()

    } catch (error) {
        res.status(400).send(error.message)
    }
}

module.exports = {
    categoryValidation,
    subCategoryValidation,
    loginValidation
    
}