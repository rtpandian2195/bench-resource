require('dotenv').config()
const jwt = require('jsonwebtoken')


const auth = async (req, res, next) => {
    // check header
    const authToken = req.headers['authorization'];
    const token = authToken && authToken.split(" ")[1];

  
    try {
      const payload = jwt.verify(token, process.env.JWT_TOKEN)
      console.log(payload);

    //   const payload1 = jwt.verify(token, process.env.JWT_REFRESH_TOKEN)
    //   console.log(payload1);
      // attach the user to the job routes
      res.locals.userEmail = payload?.email;
      console.log("1", res.locals.userEmail);
      next()
    } catch (error) {
        return res.status(500).send({ message : 'Authentication invalid'})
    }
  }
  
  module.exports = {auth}