const express = require('express')
const app = express()
const cookieParser = require( "cookie-parser");
const cors = require('cors')
const bodyparser = require('body-parser')
const path = require('path')


require('dotenv').config()

app.use(express.json())
app.use(cookieParser())
app.use(cors())
app.use(bodyparser.json()); 
app.use(bodyparser.urlencoded({extended: true}));
app.use('/uploads', express.static(path.join(__dirname,'uploads')))



const port = process.env.PORT

const categoryrouter = require('./routes/Routes');
// const subcategoryrouter = require('./routes/sub_category');

app.use('/', categoryrouter);


app.listen(port, () =>  {
    console.log(`port is running ${port}`);
})