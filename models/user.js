const Sequelize = require('sequelize')

const {sequelize} = require('./index')

const user = sequelize.define('user', {
  email: { type: Sequelize.STRING },
  password: { type: Sequelize.STRING },
})

//user.sync({force:false, alter: true });

user.sync().then(() => {
  console.log('user Table in sync now');
});

// user.associate = (models) => {
//   user.hasMany(models.post, {foreignKey : 'user_id'})
// }


module.exports = user