const Sequelize = require('sequelize')

const {sequelize} = require('./index')

const Auto = sequelize.define('auto', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    url: {
        type: Sequelize.TEXT
    },
    name : {
        type: Sequelize.TEXT
    },
    price: {
        type: Sequelize.FLOAT,
    },
    currency : {
        type: Sequelize.TEXT
    },
    availability : {
        type: Sequelize.STRING
    },
    brand : {
        type: Sequelize.STRING
    },
    mainImage : {
        type: Sequelize.TEXT
    },
    description : {
        type: Sequelize.TEXT
    },
    additionalProperty : {
      type: Sequelize.JSON
    },
    // color : {
    //     type: Sequelize.STRING
    //   },
    // productdimensions : {
    //   type: Sequelize.TEXT
    // },
    ratingValue  : {
        type: Sequelize.FLOAT,
       
    },
    reviewCount : {
        type: Sequelize.BIGINT
    },
    Tag : {
      type: Sequelize.STRING
    },
    category_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'categories',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    sub_category_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'sub_categories',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    }
})

Auto.sync().then(() => {
  console.log('Auto Table in sync now');
});

Auto.associate = (models) => {
  Auto.belongsTo(models.category, {foreignKey : 'category_id'})
  Auto.belongsTo(models.sub_category, {foreignKey : 'sub_category_id'})
}

//category.sync({force:false, alter: true });




module.exports = Auto