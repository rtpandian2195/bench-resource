const {Sequelize, DataTypes} = require('sequelize')
//const category = require('./category')

const {sequelize} = require('./index')

const sub_category = sequelize.define('sub_category', {
  name: { type: Sequelize.STRING },
  category_id : { type: Sequelize.INTEGER, references: {
    model: 'categories',
    key: 'id',
  },
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE', }
})


sub_category.sync().then(() => {
  console.log('sub_category Table in sync now');
});


module.exports = sub_category



