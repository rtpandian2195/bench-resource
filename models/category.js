const {Sequelize} = require('sequelize')
const sub_category = require('./sub_category')

const {sequelize} = require('./index')

const category = sequelize.define('category', {
  name: { type: Sequelize.STRING }
})

category.sync().then(() => {
  console.log('category Table in sync now');
});



category.hasMany(sub_category, {foreignKey : 'category_id'})



module.exports = category



 


