const Sequelize = require('sequelize')

const {sequelize} = require('./index')

const Best = sequelize.define('best', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: Sequelize.TEXT
      },
      subtitle : {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      metadescription : {
        type: Sequelize.TEXT
      },
      metakeyword : {
        type: Sequelize.TEXT
      },
      Tag : {
        type: Sequelize.TEXT
      },
      MonthDate : {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });


    
Best.sync().then(() => {
    console.log('Best Table in sync now');
  });

module.exports = Best