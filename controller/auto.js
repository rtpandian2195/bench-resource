const Sequelize = require('sequelize')
const Auto = require('../models/auto')
const fs  = require('fs')
const path = require('path')
const Category = require('../models/category')
const Sub_category = require('../models/sub_category')

const Postdata = async(req,res) => {
    // try {
       
    //     const data = fs.readFileSync('auto.json');
    //     const autos = JSON.parse(data);

        
    //     for (const item of autos) {
    //         const {price,availability, currency } = item.offers[0];
    //         const { ratingValue, reviewCount } = item.aggregateRating;
            
    //         const updatedAutos = autos.map(auto => {
    //             return {
    //               ...auto,
    //               price,availability, currency, ratingValue, reviewCount,
    //               category_id: 1,
    //               sub_category_id: 1
    //             }
    //           });
              
    //           // Sync the model with the database and insert the data
    //         const insertedAutos = await Auto.bulkCreate(updatedAutos);
           
    //         return res.status(200).json({
    //           status: 'Data inserted successfully',
    //           products: insertedAutos
    //         });
    //     }
       
    // } catch (error) {
    //     res.status(500).send(error.message)
    // }
}

const getData = async(req,res) => {
    try {
        const fs = require('fs');
        const data = fs.readFileSync('ALL-json/Jeans for men.json');
        const objects = JSON.parse(data);

        console.log(objects.length);

        //file
        let maxCount = 0;
        let lastBreadcrumbNameMap = new Map();

        objects.forEach(obj => {
          if (obj.breadcrumbs && obj.breadcrumbs.length > 0) {
            const lastBreadcrumb = obj.breadcrumbs[obj.breadcrumbs.length - 1];
            const lastBreadcrumbName = lastBreadcrumb.name;
            const count = lastBreadcrumbNameMap.has(lastBreadcrumbName)
              ? lastBreadcrumbNameMap.get(lastBreadcrumbName) + 1
              : 1;
            lastBreadcrumbNameMap.set(lastBreadcrumbName, count);
            maxCount = Math.max(maxCount, count);
          }
        });

        // Find the last name with the maximum count
        let maxLastName;
        lastBreadcrumbNameMap.forEach((count, name) => {
          if (count === maxCount) {
            maxLastName = name;
          }
        });

        // Filter the objects that have the maximum number of occurrences of the breadcrumbs' last name
        const filteredData = objects.filter(obj => {
          if (obj.breadcrumbs && obj.breadcrumbs.length > 0) {
            const lastBreadcrumb = obj.breadcrumbs[obj.breadcrumbs.length - 1];
            const lastBreadcrumbName = lastBreadcrumb.name;
            return lastBreadcrumbName === maxLastName;
          }
          return false;
        });

        console.log(filteredData.length);

        const filename = `${maxLastName}.json`;
        const folderPath = path.join(__dirname, '../collection');
        const filePath = path.join(folderPath, filename);

        fs.mkdirSync(folderPath, { recursive: true });

        fs.writeFile(filePath, JSON.stringify(filteredData), err => {
          if (err) {
            console.error(err);
          } else {
            console.log(`Data written to ${filePath}`);
          }
        });
        //file

        return res.status(200).json({
            status: 'Data of monitor filter successfully',
            products: filteredData
          });
    } catch (error) {
        res.status(500).send(error.message)
    }
}

const Topdata = async(req,res) => {
    try {
        const fs = require('fs');

      
       //4.5
       //====
     //  const sss = JSON.parse(fs.readFileSync('monitor.json', 'utf8'));
       
       const products = JSON.parse(fs.readFileSync('collection/Jeans.json', 'utf8'));

     
     //instock

       const filteredData = products.filter(item => {
           return item.offers && item.offers.some(offer => offer.availability === "InStock");
       });

    //top price
       const toppriceproducts =  filteredData.sort((a, b) => {
           const priceA = parseFloat(a.offers[0].price);
           const priceB = parseFloat(b.offers[0].price);
         
           if (priceA > priceB) {
             return -1;
           } else if (priceA < priceB) {
             return 1;
           } else {
             return 0;
           }
         });

        // console.log("3", filteredData.length);
        //console.log("4", toppriceproducts.length);
    //rating >=4.5 & review count > 100
        const topRatedProducts = toppriceproducts.filter(obj => {
           if (obj.aggregateRating && obj.aggregateRating.ratingValue > 4.5 && obj.aggregateRating.reviewCount > 100) {
             return true;
           } else {
             return false;
           }
         });
    // //sort rating
        const TopRatingProducts = topRatedProducts.sort((a,b) => b.aggregateRating.ratingValue - a.aggregateRating.ratingValue)

       // console.log("0");

        const TopItems = TopRatingProducts.slice(0,10)
     
        const Topitems = TopItems.map(auto => {
            const { price, availability, currency } = auto.offers[0];
            const { ratingValue, reviewCount } = auto.aggregateRating;
            // const { additionalProperty } = auto;

            // var productdimensions,color;

            
            // for (const property of additionalProperty) {
            //   if (property.name === 'product dimensions') {
            //     productdimensions = property.value;
               
            //   }else if (property.name === 'color')  {
            //     color = property.value;
                
            //   }
            // }
            console.log("0", auto.additionalProperty);
            return {
                ...auto,
                price,
                availability,
                currency,
                ratingValue,
                reviewCount,
                // productdimensions,
                // color,
              
            }

           
        });

      
   // 123 
 const final =  (async () => {
    const categoryMap = {};
  
    for (const auto of Topitems) {
      if (auto.breadcrumbs && auto.breadcrumbs.length > 0) {
        const categoryName = auto.breadcrumbs[0].name;
        let category = categoryMap[categoryName];
        if (!category) {
          category = await Category.findOne({ where: { name: categoryName } });
          if (!category) {
            category = await Category.create({ name: categoryName });
          }
          categoryMap[categoryName] = category;
        }
        
        const subCategoryName = auto.breadcrumbs[1].name;
        let subcategory = await Sub_category.findOne({ where: { name: subCategoryName } });
        if (!subcategory) {
          subcategory = await Sub_category.create({ name: subCategoryName, category_id: category.id });
        }

       // console.log("100", additionalProperty)
        await Auto.create({
          // Set other fields for the Auto record
          ...auto,
          category_id: category.id,
          sub_category_id: subcategory.id,
          Tag: auto.breadcrumbs[auto.breadcrumbs.length - 1].name, // Extract the last name from breadcrumbs
          price: auto.price,
          availability: auto.availability,
          currency: auto.currency,
          ratingValue: auto.ratingValue,
          reviewCount: auto.reviewCount,
         // productdimensions: auto.productdimensions,
          //color: auto.color,
        });

     // console.log("230",Tag );
      } 
    }
  })();

 
  
      
  return res.status(200).json({
    status: 'Data inserted successfully',
    products: Topitems
});
  
   } catch (error) {
       return  res.status(500).send(error.message)
   }
}

const getDataall = async(req,res) => {
    try {
        const data = await Auto.findAll()

        return res.status(200).json({
            status: "ok",
            Data : data
        })
    } catch (error) {
        res.status(500).send(error.message)
    }
}

const UpdateData = async(req,res) => {
    try {
        const {id} = req.params;
   
    const {url, name,price, currency,brand,
        availability,mainImage,description,ratingValue,reviewCount} = req.body

        const product = await Auto.findOne({
            where : {
                id : id
            }
        })

        if(!product) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        product.url = url
        product.name = name
        product.price = price
        product.currency = currency
        product.brand = brand
        product.availability = availability
        product.mainImage = mainImage
        product.description = description
        product.ratingValue = ratingValue
        product.reviewCount = reviewCount

        product.save()

        return res.status(200).json({
            status : "Updated",
            message : product
            
        })
        
    } catch (error) {
       return  res.status(500).send(error.message)
    }
}

const DeleteData = async(req,res) => {
    try {
        const {id} = req.params

        const product = await Auto.findOne({
            where : {
                id : id
            }
        })

        if(!product) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        product.destroy()

        return res.status(200).json({
            status : "deleted Successfully",
           
            
        })

    } catch (error) {
        return  res.status(500).send(error.message)
    }
}


const TagData = async(req,res) => {

      Auto.findAll({
        attributes: [[Sequelize.fn('DISTINCT', Sequelize.col('Tag')), 'uniqueTags']],
      })
        .then((autos) => {
          const uniqueTags = autos.map((auto) => auto.dataValues.uniqueTags);
          //console.log(uniqueTags);
          // Or iterate over the unique tags and display each value separately
          uniqueTags.forEach((tag) => {
           // console.log(tag);
          });
      
          // Send the unique tags in the response
          return res.status(200).json({
            status: 'success',
            tags: uniqueTags,
          });
        })
        .catch((err) => {
          console.error('Error retrieving data:', err);
          // Handle the error and send an error response
          return res.status(500).json({
            status: 'error',
            message: 'Error retrieving data',
          });
        });
}

const getDate = async(req,res) => {
  Auto.findAll({
    attributes: [
      [Sequelize.fn('DISTINCT', Sequelize.fn('DATE_FORMAT', Sequelize.col('createdAt'), '%M %Y')), 'formattedDate'],
    ],
  })
    .then((autos) => {
      const uniqueDates = autos.map((auto) => auto.dataValues.formattedDate);
     // console.log(uniqueDates);
      // Or iterate over the unique dates and display each value separately
      uniqueDates.forEach((date) => {
       // console.log(date);
      });
  
      // Send the unique dates in the response
      return res.status(200).json({
        status: 'success',
        dates: uniqueDates,
      });
    })
    .catch((err) => {
      console.error('Error retrieving data:', err);
      // Handle the error and send an error response
      return res.status(500).json({
        status: 'error',
        message: 'Error retrieving data',
      });
    });
}

module.exports = {
    Postdata,
    getData,
    getDataall,
    UpdateData,
    DeleteData,
    Topdata,
    TagData,
    getDate
}