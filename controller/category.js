const category = require('../models/category')
const sub_category = require('../models/sub_category')
//const db = require('../models')



const createCategory = async(req,res) => {
    try {
        const {name} = req.body

        const data = await category.create({
            name
        })

      //  console.log("data",data);

       return res.status(201).json({data})

    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const getCategory = async(req,res) => {
    try {
        const data = await category.findAll({
            include : [
                {
                    model : sub_category,
                    // attributes : ['name']
                }
              
            ]
        })

        return res.status(200).json({
            status: "ok",
            category : data
        })
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const getDetailCategory = async(req,res) => {
    try {
        const {id} = req.params

        const data = await category.findOne({
            where : {
                id : id
            }
        })

        if(!data) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        return res.status(200).json({
            status : "OK",
            category : data
        })

    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const UpdateCategory = async(req,res) => {
    try {
        const {id} = req.params
        const {name} = req.body

        const data = await category.findOne({
            where : {
                id : id
            }
        })

        if(!data) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        data.name = name

        await data.save()

        return res.status(200).json({
            status : "Updated",
            message : data
            
        })
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const DeleteCategory = async(req,res) => {
    try {
        const {id} = req.params
       
        const data = await category.findOne({
            where : {
                id : id
            }
        })

        if(!data) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        await data.destroy()

        return res.status(200).json({
            status : "Deleted Successfully",
            
        })
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const getAllCategory = async(req,res) => {
    try {
       
        const sub_categories = await category.findAll({
            include: { model: sub_category }
          });
          
          res.status(200).json({
            message: "ok",
            data: sub_categories
          });
       
    } catch (error) {
        res.status(400).send(error.message)
    }
}

module.exports = {
    createCategory,
    getCategory,
    getDetailCategory,
    UpdateCategory,
    DeleteCategory,
    getAllCategory
}