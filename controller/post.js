const post = require('../models/post');
const User = require('../models/user')


const createPost = async(req,res) => {
    try {
        const data = req.body
        console.log("1");
        data["image"] = req.file.originalname
        console.log("2");
        data["filePath"] = req.file.path
        data["fileType"] =  req.file.mimetype
        data["fileSize"] = fileSizeFormatter(req.file.size, 2)
        const Post = await post.create({...data})

        res.status(400).json({
            message : "created",
            data : Post
        })
    } catch (error) {
        res.status(400).send(error.message)
    }
}

const fileSizeFormatter = (bytes,decimal) => {
    if(bytes ===0) {
        return '0 bytes';
    }
    const dm = decimal || 2;
    const sizes =['Bytes', 'KB','MB','GB','TB','PB','EB','YB','ZB'];
    const index =Math.floor(Math.log(bytes) / Math.log(1000));
    return parseFloat((bytes/Math.pow(1000,index)).toFixed(dm)) + ' ' + sizes[index];
}

const Userpost = async(req,res) => {
    try {
        const email = res.locals.userEmail
        const user = await User.findOne({
            where: {
                email : email
            }
        })

        const Post = await post.findAll({
            where : {
               user_id  : user.id
            },
        })

        console.log("user",user);
        res.status(200).json({
            message : "ok",
            data : Post
        })
    } catch (error) {
        res.status(400).send(error.message)
    }
}


module.exports = {
    createPost,
    Userpost
}
    