const {Sequelize, Op } = require('sequelize')
const Auto = require('../models/auto')
const Best = require('../models/best')

const topbest = async(req,res) => {
    try {
        const {title,subtitle,description,metadescription,metakeyword,Tag,MonthDate} = req.body

        const data = await Best.create({
            title,subtitle,description,metadescription,metakeyword,Tag,MonthDate
        })

      //  console.log("data",data);

       return res.status(201).json({data})

    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const bestData = async(req,res) => {
    try {
      
            //
            const { Tag, MonthDate } = req.body;

            const startDate = new Date(MonthDate);
            const endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);

            const autos = await Auto.findAll({
            where: {
                Tag: Tag,
                createdAt: {
                [Op.between]: [startDate, endDate],
                },
            },
            });

            return res.status(200).json({
            status: 'Data of monitor filter successfully',
            products: autos,
            });
            //
      
     
    } catch (error) {
        res.status(500).send(error.message)
    }
}

module.exports = {
    bestData,
    topbest
}