const sub_category = require('../models/sub_category')


const createSubCategory = async(req,res) => {

    try {
        const {name, category_id} = req.body

        const subcategory  = await sub_category.create({
            name,
            category_id
        })
    
        res.status(201).json({
            message : "created",
            data : subcategory
        }) 
    } catch (error) {
        res.status(400).send(error.message)
    }
}

const getSubCategory = async(req,res) => {
    try {
        const data = await sub_category.findAll()

        return res.status(200).json({
            status: "ok",
            category : data
        })
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const getDetailSubCategory = async(req,res) => {
    try {
        const {id} = req.params

        const data = await sub_category.findOne({
            where : {
                id : id
            }
        })

        if(!data) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        return res.status(200).json({
            status : "OK",
            category : data
        })

    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const UpdateSubCategory = async(req,res) => {
    try {
        const {id} = req.params
        const {name,category_id} = req.body

        const data = await sub_category.findOne({
            where : {
                id : id
            }
        })

        console.log("data", data);

        if(!data) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        data.name = name;
        data.category_id = category_id;

        console.log("name", name);
       
        await data.save()

        return res.status(200).json({
            status : "Updated",
            message : data
            
        })
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

const DeleteSubCategory = async(req,res) => {
    try {
        const {id} = req.params
       
        const data = await sub_category.findOne({
            where : {
                id : id
            }
        })

       

        if(!data) {
            return res.status(400).json({
                status : "Not Found"
            })
        }

        await data.destroy()

        return res.status(200).json({
            status : "Deleted Successfully",
            
        })
    } catch (error) {
        return res.status(400).send(error.message)
    }
}

module.exports = {
    createSubCategory,
    getSubCategory,
    getDetailSubCategory,
    UpdateSubCategory,
    DeleteSubCategory
}