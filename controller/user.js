const user = require('../models/user')
const post = require('../models/post')

const PasswordHelper = require('../helper/PasswordHelper')

const Helper = require('../helper/Helper')


const login = async(req,res) => {
    try {
        const {email,password} = req.body
      

        const User = await user.findOne({
            where : {
                email : email
            }
        })

        // console.log("email", email);
        // console.log("User", User);

        const hash = await PasswordHelper.PassworHashing(password)

        if(!User) {
           const data=  await user.create({
                email,
                password :hash
            })

           // console.log("email1", email);

            const dataUser = {
                email : data.email,
               
            }

           // console.log("email2",email);

            const token = await Helper.GenerateToken(dataUser)
            const refreshToken =await Helper.GenerateRefreshToken(dataUser) 

            data.accessToken = refreshToken;
            await data.save();

            res.cookie('refreshToken', refreshToken, {
                httpOnly : true,
                maxAge : 24 * 60 * 60 * 1000
            })
        

            const responseUser = {
                email : data.email,
                token : token,
    
            }
    
           
            return res.status(200).json({
                status : "Login Successfully",
                user :  responseUser

            })
           
        }

        
        else {
        
            const User = await user.findOne({
                where : {
                    email : email
                }
            });
          
            
            const matched = await PasswordHelper.PasswordCompare(password,User.password) 
            console.log("password",password);
            console.log("password", User.password);
            if(!matched) {
                return res.status(401).send("email or password Invalid")
            }
            
            
            const dataUser = {
                email : User.email,
               
            }

            const token = await Helper.GenerateToken(dataUser)
            const refreshToken =await Helper.GenerateRefreshToken(dataUser) 

            User.accessToken = refreshToken;
            await User.save();

            res.cookie('refreshToken', refreshToken, {
                httpOnly : true,
                maxAge : 24 * 60 * 60 * 1000
            })

            console.log("refreshToken", refreshToken);
         

            const responseUser = {
                email : User.email,
                token : token,
    
            }
    

            return res.status(200).json({
                status : "Login Successfully",
                user :  responseUser

            })
        }

    } catch (error) {
        res.status(400).send(error.message)
    }
}

const RefreshToken = async(req,res) => {
    try {
        const refreshToken = req.cookies?.refreshToken;
        // res.send(refreshToken)
         console.log(refreshToken);

         if(!refreshToken) {
            return res.status(401).send("1Unauthorized")
        }

        console.log("1");
        

        const decodedUser = Helper.ExtractToken(refreshToken)
        console.log("decodedUser",decodedUser);
        
        if(!decodedUser) {
            return res.status(401).send("Unauthorized")
        }

        console.log("2");
        

        const token = Helper.GenerateToken({
            email : decodedUser.email,
          
        })

        const resultUser = {
            email : decodedUser.email,
            token : token
        }

        return res.status(200).json({
            status : "ok",
            data : resultUser
        })
       
    } catch (error) {
        res.send(error.message)
    }
}

const Userpost = async(req,res) => {
    try {
        const email = res.locals.userEmail
        const User = await user.findOne({
            where: {
                email : email
            }
        })

        const Post = await post.findAll({
            where : {
               user_id  : User.id
            },
        })

        console.log("user",User);
        res.status(200).json({
            message : "ok",
            data : Post
        })
    } catch (error) {
        res.status(400).send(error.message)
    }
}

const UserDetail = async(req,res) => {
    try {
        const email = res.locals.userEmail
        const User = await user.findOne({
            where: {
                email : email
            }
        })

        if(!user) {
            return res.status(404).send("User not found")
        }

        User.password = "";
        User.accessToken = "";

        console.log("user",User);
        res.status(200).json({
            message : "ok",
            data : User
        })

    } catch (error) {
        res.status(400).send(error.message)
    }
}

const UserLogout  = async(req,res) => {
    try {
        const refreshToken = req.cookies?.refreshToken;
        // res.send(refreshToken)
         console.log(refreshToken);

        if(!refreshToken) {
            return res.status(200).json({
                status : "1Logout Successfully"
            })
        }

        const email = res.locals.userEmail
        const User = await user.findOne({
            where: {
                email : email
            }
        })
        if(!User) {
            res.clearCookie("refreshToken")
            return res.status(200).json({
                status : "2user logout"
            })
        }
        await User.update({accessToken : null}, {where: {email : email}})
       
        res.clearCookie("refreshToken")

        return res.status(200).json({
            status : "3user logout"
        })
    } catch (error) {
        res.status(400).send(error.message)
    }
}


module.exports = {
    login,
    RefreshToken,
    Userpost,
    UserDetail,
    UserLogout,
  

}