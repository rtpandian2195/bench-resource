'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('autos', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      url: {
        type: Sequelize.TEXT
      },
      name : {
        type: Sequelize.TEXT
      },
      price: {
        type: Sequelize.BIGINT
      },
      currency : {
        type: Sequelize.TEXT
      },
      availability : {
        type: Sequelize.STRING
      },
      brand : {
        type: Sequelize.STRING
      },
      mainImage : {
        type: Sequelize.TEXT
      },
      description : {
        type: Sequelize.TEXT
      },
      additionalProperty : {
        type: Sequelize.JSON
      },
      // color : {
      //   type: Sequelize.STRING
      // },
      // productdimensions : {
      //   type: Sequelize.TEXT
      // },
      ratingValue  : {
        type: Sequelize.FLOAT
      },
      reviewCount : {
        type: Sequelize.BIGINT
      },
      Tag : {
        type: Sequelize.STRING
      },
      category_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'categories',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      sub_category_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'sub_categories',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};