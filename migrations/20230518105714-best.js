'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('best', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: Sequelize.TEXT
      },
      subtitle : {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      metadescription : {
        type: Sequelize.TEXT
      },
      metakeyword : {
        type: Sequelize.TEXT
      },
      Tag : {
        type: Sequelize.TEXT
      },
      MonthDate : {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
